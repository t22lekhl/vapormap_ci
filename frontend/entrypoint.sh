#!/bin/sh
envsubst '${VAPORMAP_BACKEND}, ${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json
envsubst '${VAPORMAP_URL_SERVERNAME}, ${VAPORMAP_URL_PORT}, ${VAPORMAP_FRONTEND_ROOT}' < nginx.conf.template > nginx.conf
cp nginx.conf /etc/nginx/conf.d/default.conf

exec "$@"
